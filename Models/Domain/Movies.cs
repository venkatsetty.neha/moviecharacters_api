﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharecters_API.Models.Domain
{
    [Table("Movies")]
    public class Movies
    {
       //PK
        public int Id { get; set; }
        //Feilds
        [Required]
        [MaxLength(50)]
        public string Title { get; set; }
        public string Genre { get; set; }
        [Required]
        public int ReleaseYear { get; set; }
        [Required]
        [MaxLength(50)]
        public string Director{ get; set; }
        public string Picture { get; set; }
        public string Trailer{ get; set; }
        // Relationships(many to many)
        public ICollection<Characters> Characters { get; set; }
        public int FranchiseId { get; set; }
        public Franchises Franchise { get; set; }



    }
}
