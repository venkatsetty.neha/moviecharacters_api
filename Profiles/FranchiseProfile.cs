﻿using AutoMapper;
using MovieCharecters_API.Models.Domain;
using MovieCharecters_API.Models.DTO.Franchises;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharecters_API.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchises, FranchiseReadDTO>()
                    .ForMember(fdto => fdto.MovieId, opt => opt
                    .MapFrom(f => f.Movies.Select(f => f.Id).ToArray()))
                    .ForMember(fdto => fdto.MovieName, opt => opt
                    .MapFrom(f => f.Movies.Select(f => f.Title).ToArray()))
                    .ReverseMap();

            CreateMap<Franchises, FranchiseCreateDTO>().ReverseMap();

            CreateMap<Franchises, FranchiseEditDTO>().ReverseMap();
        }
}
}

