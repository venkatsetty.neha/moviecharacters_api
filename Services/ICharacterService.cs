﻿using MovieCharecters_API.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharecters_API.Services
{
    public interface ICharacterService
    {
        public Task<IEnumerable<Characters>> GetAllCharactersAsync();
        public Task<Characters> GetSpecificCharacterAsync(int id);
        public Task<Characters> AddCharacterAsync(Characters character);
        public Task UpdateCharacterAsync(Characters character);
        public Task DeleteCharacterAsync(int id);
        public bool CharacterExists(int id);
    }
}