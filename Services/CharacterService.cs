﻿using Microsoft.EntityFrameworkCore;
using MovieCharecters_API.Models;
using MovieCharecters_API.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharecters_API.Services
{
    public class CharacterService : ICharacterService
    {
        private readonly CharacterDbContext _context;
        public CharacterService(CharacterDbContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Get All Movies
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Characters>> GetAllCharactersAsync()
        {
            return await _context.Characters
                           .Include(c => c.Movies)
                           .ToListAsync();
        }
        /// <summary>
        /// Get a character by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Characters> GetSpecificCharacterAsync(int id)
        {
            Characters character = await _context.Characters
               .Include(c => c.Movies)
               .Where(m => m.Id == id)
               .FirstAsync();
            return character;
        }
        /// <summary>
        /// Post a Character
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        public async Task<Characters> AddCharacterAsync(Characters character)
        {
            _context.Characters.Add(character);
            await _context.SaveChangesAsync();
            return character;
        }
        /// <summary>
        /// Checks whether character exits
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool CharacterExists(int id)
        {
            return _context.Characters.Any(c => c.Id == id);
        }
        /// <summary>
        /// update Character
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        public async Task UpdateCharacterAsync(Characters character)
        {
            _context.Entry(character).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        /// <summary>
        /// delete a Character
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteCharacterAsync(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();
        }
    }
}


