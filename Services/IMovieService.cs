﻿using MovieCharecters_API.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharecters_API.Services
{
    public interface IMovieService
    {
        public Task<IEnumerable<Movies>> GetAllMoviesAsync();
        public Task<Movies> GetSpecificMovieAsync(int id);
        public Task<Movies> AddMovieAsync(Movies movie);
        public Task UpdateMovieAsync(Movies movie);
        public Task<bool> UpdateMovieCharactersAsync(int id, int[] characterIds);
        public Task DeleteMovieAsync(int id);
        public bool MovieExists(int id);


    }
}
