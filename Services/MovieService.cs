﻿using Microsoft.EntityFrameworkCore;
using MovieCharecters_API.Models;
using MovieCharecters_API.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharecters_API.Services
{
    public class MovieService : IMovieService
        {
            private readonly CharacterDbContext _context;
            public MovieService(CharacterDbContext context)
            {
                _context = context;
            }
        /// <summary>
        /// get all Movies
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Movies>> GetAllMoviesAsync()
        {
            return await _context.Movies
             .Include(m => m.Characters)
             .Include(m => m.Franchise)
             .ToListAsync();
        }
        /// <summary>
        ///  get movie by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Movies> GetSpecificMovieAsync(int id)
        {
               Movies movie = await _context.Movies
                           .Include(m => m.Characters)
                           .Where(m => m.Id == id)
                           .FirstAsync();
               return movie;
        }
        /// <summary>
        /// Add a movie
        /// </summary>
        /// <param name="movie"></param>
        /// <returns></returns>
        public async Task<Movies> AddMovieAsync(Movies movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();
            return movie;
        }
        /// <summary>
        /// Delete a movie
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteMovieAsync(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
        }
        /// <summary>
        /// Check whether movie exits
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool MovieExists(int id)
        {
            return _context.Movies.Any(m => m.Id == id);
        }
        /// <summary>
        /// update a movie
        /// </summary>
        /// <param name="movie"></param>
        /// <returns></returns>
        public async Task UpdateMovieAsync(Movies movie)
        {
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        /// <summary>
        /// update characters in a movie
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characterIds"></param>
        /// <returns></returns>
        public async Task<bool> UpdateMovieCharactersAsync(int id, int[] characterIds)
        {
            Movies movieToUpdateCharacters = await _context.Movies
                .Include(m => m.Characters)
                .Where(m => m.Id == id)
                .FirstAsync();
            List<Characters> characters = await _context.Characters
                .Where(c => characterIds
                .Contains(c.Id))
                .ToListAsync();
            if (characterIds.Length > characters.Count)
            {
                return false;
            }
            movieToUpdateCharacters.Characters = characters;
            await _context.SaveChangesAsync();
            return true;
        }

    }
}
